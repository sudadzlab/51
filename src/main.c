/**
 * *********************************************
 * 
 * 8051 blink demo
 * 
 * PIN: P11
 * 
 * *********************************************
*/



#include "STC15F2K60S2.H"  //用此头文件直接代替REG52.H，请进行两个头文件对比
#include <DS1302.H>
#include <LCD1602.h>
#include "stdio.h"
#include "DateSetting.h"
#include "Alarm.h"

// 常量宏定义
#define MODE_MAX 			    	3				/*界面数*/

// 变量声明
bit 				key_flag 		= 	0;			// 键盘扫描标志位
unsigned char 		key_value 		= 	0xff;		// 键盘读取值
unsigned char 		mode			=	0;			// 界面模式
bit 				cls				=	1;			// 清屏标志位
unsigned char		setting_mode    = 	0;
xdata unsigned int 	year;
xdata unsigned char month, day, week;
xdata unsigned char hour, minute, second;
xdata unsigned char hour_flag;
xdata unsigned int 	year_temp;
xdata unsigned char	month_temp, day_temp, week_temp;
xdata char 			hour_temp, minute_temp, second_temp;
xdata unsigned char	hour_flag_temp;
xdata unsigned char	hour_flag_temp_change = 1;
xdata unsigned char alram_setting_count = 0;
xdata unsigned char workDay_show [7] = {'1','2','3','4','5','6','7'};
xdata unsigned char alarm_hour, alarm_minute, alarm_workDay, alarm_workState;
xdata char 			alarm_hour_temp, alarm_minute_temp, alarm_workDay_temp, alarm_workState_temp;
xdata unsigned char setting_show_flag;
code unsigned char week_show[7][4]={"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};


// 函数声明
void Timer0Init				(void);					//Timer0定时器初始化
void read_keyboard			(void);					//读取矩阵键盘键值
void key_action				(void);					//通过判断key_value值，并根据不同的界面执行不同的操作
void Alarm					(bit flag);				//警报操作
void display				(void);					//LCD界面展示
void Delay100ms				(void);					//延时100ms，为开机动画准备
void PowerOn				(void);					//开机动画显示



/**********************************************************
 * @name Timer0
 * @brief 	Timer0中断服务程序
 * @param void
 * @return none
 **********************************************************/
void Timer0(void) interrupt 1
{
	
	static unsigned char icount = 0, setting_show=0;		// 定时辅助计数
	if(icount>=50)
	{
		key_flag = 1;
		icount=0;
	}
	if(setting_show >= 200){
		setting_show_flag = !setting_show_flag;
		setting_show = 0;
	}
	setting_show++;
	icount++;
}

/**********************************************************
 * @name Timer0Init
 * @brief 	Timer0定时器初始化
 * @param void
 * @return none
 **********************************************************/
void Timer0Init(void)		//1毫秒@12.000MHz
{
	AUXR |= 0x80;		//定时器时钟1T模式
	TMOD &= 0xF0;		//设置定时器模式
	TL0 = 0x20;		//设置定时初值
	TH0 = 0xD1;		//设置定时初值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时
	ET0=1;
	EA=1;
}

/**********************************************************
 * @name read_keyboard
 * @brief 	读取矩阵键盘键值，并储存读取值到全局变量key_value
 * 			转接板中使用P42和P44代替8051引脚
 *			顺序中的P36和P37引脚
 * @param void
 * @return none
 **********************************************************/
void read_keyboard(void)
{
    static unsigned char hang;
	static unsigned char key_state=0;	
	switch(key_state)
	{
		case 0:
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 != 0x0f) //有按键按下
			key_state=1;	
		}break;
		case 1:
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 != 0x0f) //有按键按下
			{
				if(P30 == 0)hang = 1;
				if(P31 == 0)hang = 2;
				if(P32 == 0)hang = 3;
				if(P33 == 0)hang = 4;//确定行	    
				switch(hang){
					case 1:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=0;key_state=2;}
						else if(P42 == 0) {key_value=1;key_state=2;}
						else if(P35 == 0) {key_value=2;key_state=2;}
						else if(P34 == 0) {key_value=3;key_state=2;}
					}break;
					case 2:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=4;key_state=2;}
						else if(P42 == 0) {key_value=5;key_state=2;}
						else if(P35 == 0) {key_value=6;key_state=2;}
						else if(P34 == 0) {key_value=7;key_state=2;}
					}break;
					case 3:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=8;key_state=2;}
						else if(P42 == 0) {key_value=9;key_state=2;}
						else if(P35 == 0) {key_value=10;key_state=2;}
						else if(P34 == 0) {key_value=11;key_state=2;}
					}break;
					case 4:{P3 = 0xf0; P42 = 1; P44 = 1;
						if(P44 == 0) {key_value=12;key_state=2;}
						else if(P42 == 0) {key_value=13;key_state=2;}
						else if(P35 == 0) {key_value=14;key_state=2;}
						else if(P34 == 0) {key_value=15;key_state=2;}
					}break;
				}	
			}
			else
			{
				key_state=0;	
			}  	   
		}break;
		case 2:     
		{
			P3 = 0x0f; P42 = 0; P44 = 0;
			if(P3 == 0x0f) //按键放开
			key_state=0;	
		}break;
						
    } 
	
}

/**********************************************************
 * @name key_action
 * @brief 	通过判断key_value值，并根据不同的界面执行不同的操作
 * @param void
 * @return none
 **********************************************************/
void key_action(void)
{
	unsigned char i;
	switch(key_value)
	{
	case 0:		//切换界面
		mode++;
		if(mode >= MODE_MAX){
			mode = 0;
		}
		cls = 1;
		switch (mode)
		{
		case 1:
			year_temp 	= year;
			month_temp	= month;
			day_temp 	= day;
			week_temp 	= week;
			hour_temp 	= hour;			
			minute_temp = minute;
			second_temp = second;
			hour_flag_temp = hour_flag;
			setting_mode = 0;
			break;
		case 2:
			alarm_hour_temp			= alarm_hour;
			alarm_minute_temp		= alarm_minute;
			alarm_workDay_temp		= alarm_workDay;
			alarm_workState_temp	= alarm_workState;
			for(i=0; i<7; i++){
				if((alarm_workDay>>i)&0x01){
					workDay_show[i] = 0xff;
				}
			}			
			break;		
		default:
			break;
		}
		break;		
	case 1:
		switch (mode)
		{
		case 1:
			setting_mode += 1;			
			cls = 1;
			if(setting_mode >=6){
				setting_mode = 0;
			}
			break;
		case 2:
			alram_setting_count++;
			if(alram_setting_count >=9){
				alram_setting_count = 0;
			}
		default:
			break;
		}
		break;
	case 2:
		if(mode == 1){
			if(hour_flag_temp_change){
				hour_flag_temp_change = 0;
			}
			switch (setting_mode)
			{
			case 0:
				yearChange(&year_temp,&month_temp,&day_temp,&week_temp,1);
				break;
			case 1:
				monthChange(&year_temp,&month_temp,&day_temp,&week_temp,1);
				break;
			case 2:
				dayChange(&year_temp,&month_temp,&day_temp,&week_temp,1);
				break;
			case 3:
				hour_temp += 1;
				if(hour_flag_temp == 0x00 && hour_temp > 23){
					hour_temp = 0;
				}else if((hour_flag_temp>>1) && hour_temp == 12){					
					hour_flag_temp = hour_flag_temp==0x02?0x03:0x02;
				}else if((hour_flag_temp>>1) && hour_temp == 13){				
					hour_temp = 1;
				}
				break;
			case 4:
				minute_temp += 1;
				if(minute_temp >= 60){
					minute_temp = 0;
				}
				break;
			case 5:
				second_temp += 1;
				if(second_temp >= 60){
					second_temp = 0;
				}
				break;			
			default:
				break;
			}
		}
		if(mode == 2){
			switch (alram_setting_count)
			{
			case 0:
				alarm_hour_temp += 1;
				if(alarm_hour_temp >= 24){
					alarm_hour_temp=0;
				}
				break;
			case 1:
				alarm_minute_temp += 1;
				if(alarm_minute_temp >= 60){
					alarm_minute_temp = 0;
				}
				break;			
			default:
				workDay_show[alram_setting_count-2] = workDay_show[alram_setting_count-2]==0xff?'1'+alram_setting_count-2:0xff;
				break;
			}
		}
		break;
	case 3:
		if(mode == 1){
			switch (setting_mode)
			{
			case 0:
				yearChange(&year_temp,&month_temp,&day_temp,&week_temp,0);
				break;
			case 1:
				monthChange(&year_temp,&month_temp,&day_temp,&week_temp,0);
				break;
			case 2:
				dayChange(&year_temp,&month_temp,&day_temp,&week_temp,0);
				break;
			case 3:
				hour_temp -= 1;
				if(hour_flag_temp == 0x00 && hour_temp < 0){
					hour_temp = 23;
				}else if((hour_flag_temp>>1) && hour_temp == 11){	
					hour_flag_temp = hour_flag_temp==0x02?0x03:0x02;
				}else if((hour_flag_temp>>1) && hour_temp == 0){
					hour_temp = 12;
				}
				break;
			case 4:
				minute_temp -= 1;
				if(minute_temp < 0){
					minute_temp = 59;
				}
				break;
			case 5:
				second_temp -= 1;
				if(second_temp < 0){
					second_temp = 59;
				}
				break;			
			default:
				break;
			}
		}
		if(mode == 2){
			switch (alram_setting_count)
			{
			case 0:
				alarm_hour_temp -= 1;
				if(alarm_hour_temp < 0){
					alarm_hour_temp=23;
				}
				break;
			case 1:
				alarm_minute_temp -= 1;
				if(alarm_minute_temp < 0){
					alarm_minute_temp = 59;
				}
				break;			
			default:
				workDay_show[alram_setting_count-2] = workDay_show[alram_setting_count-2]==0xff?'0'+alram_setting_count-2:0xff;
				break;
			}
		}
		break;
	case 4:

		break;
	case 5:
		switch (mode)
		{
		case 1:
			if(setting_mode<=2){
				SetDate(year_temp, month_temp, day_temp);
			}else if(setting_mode<=5){
				SetRTC(hour_temp,minute_temp,second_temp,(hour_flag_temp>>1),(hour_flag_temp&0x01),1);
			}
			break;
		case 2:
			alarm_workDay_temp=0x00;
			for(i=0; i<7; i++){
				if(workDay_show[i]==0xff){
					alarm_workDay_temp |= 0x80;
				}
				alarm_workDay_temp>>=1;				
			}
			SetAlarm(0,alarm_hour_temp,alarm_minute_temp, alarm_workDay_temp,1,0x00);
		default:
			break;
		}
		break;
	case 6:
		SetRTCMode(hour_flag?0:1);
		cls=1;
		break;
	default:
		break;
    }
	key_value = 0xff;
}

/**********************************************************
 * @name Alarm
 * @brief 	警报操作（蜂鸣器响，继电器打开）。
 * 			当flag=1是，开启警报；flag=0时，关闭警报
 * @param flag 警报执行标志（bit）
 * @return none
 **********************************************************/
void Alarm(bit flag)
{
	P2=(P2&0X1F)|0XA0;
	P0=(P0&0XAF);
	if(flag && setting_show_flag)
		P0=P0|0X50;
	P2=P2&0X1F;
}

/**********************************************************
 * @name display
 * @brief 	LCD界面展示，在适当时间显示对应的界面
 * @param void
 * @return none
 **********************************************************/
void display(void)
{
	unsigned char i;
	if(cls)
	{
		LCD_Clear();
		cls=0;
	}
    switch(mode)
    {
    case 0:
        LCD_WriteNum(2,0,4,0,year);
		LCD_WriteChar(6,0,'-');
		LCD_WriteNum(7,0,2,0,month);
		LCD_WriteChar(9,0,'-');
		LCD_WriteNum(10,0,2,0,day);
		LCD_WriteString(13,0,week_show[week-1]);

		if(hour_flag == 0x02){
			LCD_WriteNum(4,1,2,0,hour);
		}else{
			LCD_WriteNum(4,1,2,0,hour);
		}
		LCD_WriteChar(6,1,':');
		LCD_WriteNum(7,1,2,0,minute);
		LCD_WriteChar(9,1,':');
		LCD_WriteNum(10,1,2,0,second);
					
		switch (hour_flag)
		{
		case 0x02:
			LCD_WriteString(13,1,"AM");
			break;
		case 0x03:
			LCD_WriteString(13,1,"PM");
			break;
		default:
			break;
		}
        break;
	case 1:
		if(setting_mode <= 2){
			LCD_WriteNum(2,0,4,0,year_temp);
			LCD_WriteChar(6,0,'-');
			LCD_WriteNum(7,0,2,0,month_temp);
			LCD_WriteChar(9,0,'-');
			LCD_WriteNum(10,0,2,0,day_temp);
		LCD_WriteString(13,0,week_show[week_temp-1]);
			LCD_WriteChar(5+setting_mode*3,1,'^');
		}else{
			if(hour_flag_temp == 0x02){
				LCD_WriteNum(4,0,2,0,hour_temp);
			}else{
				LCD_WriteNum(4,0,2,0,hour_temp);
			}
			LCD_WriteChar(6,0,':');
			LCD_WriteNum(7,0,2,0,minute_temp);
			LCD_WriteChar(9,0,':');
			LCD_WriteNum(10,0,2,0,second_temp);		
			switch (hour_flag_temp)
			{
			case 0x02:
				LCD_WriteString(13,0,"AM");
				break;
			case 0x03:
				LCD_WriteString(13,0,"PM");
				break;
			default:
				break;
			}			
			LCD_WriteChar(5+(setting_mode-3)*3,1,'^');
		}
		break;
	case 2:
		//LCD_WriteChar(1,0,'1');
		if(setting_show_flag){
			LCD_WriteNum(4,0,2,0,alarm_hour_temp);
			LCD_WriteChar(6,0,':');	
			LCD_WriteNum(7,0,2,0,alarm_minute_temp);
			for(i = 0; i < 7; i++){
				LCD_WriteChar(1+i*2,1,workDay_show[i]);
			}
		}
		else{
			if(alram_setting_count == 0){
				LCD_WriteChar(4,0,' ');
				LCD_WriteChar(5,0,' ');
			}else if(alram_setting_count == 1){
				LCD_WriteChar(7,0,' ');
				LCD_WriteChar(8,0,' ');
			}else{
				LCD_WriteChar((alram_setting_count-2)*2+1,1,' ');
			}
		}
		//LCD_WriteHex(10,0,2,alarm_workDay_temp,0);
		break;
    default:
        break;
    }
}

/**********************************************************
 * @name Delay100ms
 * @brief 	延时100ms，为开机动画准备
 * @param void
 * @return none
 **********************************************************/
void Delay100ms(void)	//@11.0592MHz
{
	unsigned char data i, j, k;
	i = 5;
	j = 52;
	k = 195;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

/**********************************************************
 * @name PowerOn
 * @brief 	开机动画显示
 * @param void
 * @return none
 **********************************************************/
void PowerOn(void)
{
	unsigned char i = 0, len = 0;
	len = LCD_WriteString(0, 0, "BOOT UP");
	for(i=0; i<4; i++)
	{
		LCD_WriteString(len, 0, "   ");
		Delay100ms();
		LCD_WriteString(len, 0, ".  ");
		Delay100ms();
		LCD_WriteString(len, 0, ".. ");
		Delay100ms();
		LCD_WriteString(len, 0, "...");
		Delay100ms();
	}
}

//主函数
void main(void)
{
	LCD_Init();   //LCD初始化
	Timer0Init();
	// ReadDate(&year,&month,&day,&week);
	// SetDate(year,month,day);
	// SetRTC(11,59,59,1,1,1);
	//PowerOn();
	SetAlarm(0,16,00,Mon|Tue|Wed|Thu|Fri|Sat|Sun,1,0x00);

	while(1)
	{
		if(key_flag)
        {
            key_flag = 0;
			read_keyboard();
			if(key_value != 0xFF){
				key_action();
			}
        } 
		ReadDate(&year,&month,&day,&week);
		hour_flag = ReadRTC(&hour,&minute,&second);			
		GetAlarm(0,&alarm_hour,&alarm_minute,&alarm_workDay);
		Alarm(0);	
		if(CheckAlarm(hour,minute,week,hour_flag)>=0){
			Alarm(1);
		}
		display();
	}
}


