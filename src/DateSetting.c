#include "DateSetting.h"
#include "DS1302.h"




/**
 * @name calWeek
 * @brief 计算当前日期对应的星期
 * 
 * @param year      当前年份
 * @param month     当前月份
 * @param day       当前日
 * @return 星期几（unsigned char）
 */
unsigned char calWeek( unsigned int year, unsigned char month, unsigned char day)
{
	unsigned int i;
	unsigned int day_total=0;
    unsigned char week = 0;
	for(i=ORGIN_YEAR;i<year;i++)
	{
		if(i%4==0 && (i%100!=0 || i%400==0)){
			day_total+=366;
		}
		else{
			day_total+=365;
		}
	}
	for(i=1; i<month;i++){
		if(i==1||i==3||i==5||i==7||i==8||i==10||i==12){
			day_total += 31;
		}
		else if(i==4||i==6||i==9||i==11){
			day_total += 30;
		}
		else if(i == 2 && year%4==0 && (year%100!=0 || year%400==0)){
			day_total += 29;
		}
		else if(i == 2){
			day_total += 28;
		}
	}
	day_total+=(day-1);
	week = (ORGIN_WEEK+day_total)%7;     
    return (week?week:7);
}

/**
 * @name dayChange
 * @brief 增减日
 * 
 * @param year      传入年变量指针（unsigned int*）
 * @param month     传入月变量指针（unsigned char*）
 * @param day       传入日变量指针（unsigned char*）
 * @param week      传入星期变量指针（unsigned char*）
 * @param mode      操作模式——1，日加一；0，日减一（bit）
 * @return none
 */
void dayChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode)
{
    if(mode == 1){
        *day += 1;
        *week =(*week+1)%7;
        *week = (*week?*week:7);
        if(*day <= 28){
            return;
        }
        else if(*day==29){
            if(*month ==2&&(!(*year%4==0 && (*year%100!=0 || *year%400==0))))
            {
                *day = 1;
                *month += 1;
            }
            return;
        }
        else if(*day==30){
            if(*month == 2){
                *day = 1;
                *month += 1;
            }
            return;
        }
        else if(*day==31){
            if(*month == 4||*month == 6||*month == 9||*month == 11){
                *day = 1;
                *month += 1;
            }
            return;
        }
        else
        {
            *day = 1;
            *month += 1;
            if(*month==13){                
                *month = 1;
                *year += 1;
            }
            return;
        }
    }
    else{
        *day -= 1;
        *week = (*week+6)%7;
        if(*day==0){
            *month -= 1;
            if(*month==0||*month==1||*month==3||*month==5||*month==7||*month==8||*month==10){
                *day=31;                
                if(*month == 0){
                    *month = 12;
                    *year -= 1;
                }
            }
            else if(*month==4||*month==6||*month==9||*month==11){
                *day=30;
            }
            else if(*month ==2 && *year%4==0 && (*year%100!=0 || *year%400==0)){
                *day=29;
            }
            else{
                *day=28;
            }
        }
    }
}

/**
 * @name monthChange
 * @brief 增减月份
 * 
 * @param year      传入年变量指针（unsigned int*）
 * @param month     传入月变量指针（unsigned char*）
 * @param day       传入日变量指针（unsigned char*）
 * @param week      传入星期变量指针（unsigned char*）
 * @param mode      操作模式——1，月加一；0，月减一（bit）
 * @return none
 */
void monthChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode)
{
    if(mode == 1){
        *month += 1;
        if(*month == 13){
            *month = 1;
            *year += 1;
        }
    }
    else{
        *month -= 1;
        if(*month == 0){
            *month = 12;
            *year -= 1;
        }
    }
    
    if(*day <= 28){}
    else if(*day == 29){
        if(*month ==2&&!(*year%4==0 && (*year%100!=0 || *year%400==0))){
            *day = 28;
        }
    }
    else if(*day == 30){
        if(*month ==2)
        {
            if(*year%4==0 && (*year%100!=0 || *year%400==0)){
                *day = 29;
            }
            else{
                *day = 28;
            }
        }
    }
    else if(*day==31){
        if(*month ==2)
        {
            if(*year%4==0 && (*year%100!=0 || *year%400==0)){
                *day = 29;
            }
            else{
                *day = 28;
            }
        }
        else if(*month == 4||*month == 6||*month == 9||*month == 11){
            *day = 30;
        }
    }
    *week = calWeek(*year, *month, *day);
}

/**
 * @name yearChange
 * @brief 增减年份
 * 
 * @param year      传入年变量指针（unsigned int*）
 * @param month     传入月变量指针（unsigned char*）
 * @param day       传入日变量指针（unsigned char*）
 * @param week      传入星期变量指针（unsigned char*）
 * @param mode      操作模式——1，年加一；0，年减一（bit）
 * @return none
 */
void yearChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode)
{

    if(mode == 1){
        *year += 1;
    }
    else{
        *year -= 1;
    }       
    if(!(*year%4==0 && (*year%100!=0 || *year%400==0))&&*day==29){
        *day = 28;
    }
    *week = calWeek(*year, *month, *day); 
}
