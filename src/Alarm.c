#include "Alarm.h"

#define MAX_NUM 5

typedef struct{
    unsigned char hour;
    unsigned char minute;
    unsigned char workDay;
    unsigned char workState;
}alarmDef;

static alarmDef alarm[MAX_NUM];

char SetAlarm(unsigned char selecter, unsigned char hour, unsigned char minute, unsigned char workDay, unsigned char workState, unsigned char hour_flag)
{
    unsigned char i;
    if(hour_flag == 0x02 && hour == 12){
        hour = 0;
    }else if(hour_flag == 0x03 && hour != 12){
        hour += 12;
    }
    if(selecter<MAX_NUM && selecter>=0){
        for(i = 0; i < MAX_NUM; i++){
            if(alarm[i].hour == hour && alarm[i].minute == minute){
                alarm[i].workDay = workDay;
                alarm[i].workState = workState;
                return i;
            }
        }
        alarm[selecter].hour = hour;
        alarm[selecter].minute = minute;
        alarm[selecter].workDay = workDay;
        alarm[selecter].workState = workState;
        return selecter;
    }else{
        return -1;
    }
}

char GetAlarm(unsigned char selecter, unsigned char *hour, unsigned char *minute, unsigned char *workDay)
{
    if(selecter<MAX_NUM && selecter>=0){
        *hour    = alarm[selecter].hour;
        *minute  = alarm[selecter].minute;
        *workDay = alarm[selecter].workDay;        
        return alarm[selecter].workState;
    }else{
        return -1;
    }
}

char CheckAlarm(unsigned char hour, unsigned char minute, unsigned char week, unsigned char hour_flag)
{
    unsigned char i;
    if(hour_flag == 0x02 && hour == 12){
        hour = 0;
    }else if(hour_flag == 0x03 && hour != 12){
        hour += 12;
    }
    for(i = 0; i < MAX_NUM; i++){
        if((alarm[i].workState) && ((alarm[i].workDay>>(week-1))&0x01)){
            if(alarm[i].hour == hour && alarm[i].minute == minute){
                return i;
            }
        }
    }
    return -1;
}