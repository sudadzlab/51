

#ifndef __DS1302_H__
#define __DS1302_H__


void Write_Ds1302_Byte(unsigned char temp);
void SetDate(unsigned int year, unsigned char month, unsigned char day);
void ReadDate(unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week);

void SetRTCMode(bit mode);
void SetRTC(unsigned char hour, unsigned char minute, unsigned char second, bit time_mode, bit time, bit run);
unsigned char ReadRTC(unsigned char *hour, unsigned char *minute, unsigned char *second);

#endif // !__DS1302_H__