#ifndef __ALARM_H__
#define __Alarm_h__


#define   Mon  0x01
#define   Tue  0x02
#define   Wed  0X04
#define   Thu  0x08
#define   Fri  0x10
#define   Sat  0x20
#define   Sun  0x40



char SetAlarm(unsigned char selecter, unsigned char hour, unsigned char minute, unsigned char workDay, unsigned char workState, unsigned char hour_flag);
char GetAlarm(unsigned char selecter, unsigned char *hour, unsigned char *minute, unsigned char *workDay);
char CheckAlarm(unsigned char hour, unsigned char minute, unsigned char week, unsigned char hour_flag);


#endif