#include<LCD1602.h>
#include<REG52.H>


//1602的控制引脚定义
sbit RS = P2^0;    	//寄存器选择，高电平时选择数据寄存器、低电平时选择指令寄存器
sbit RW = P2^1;		//读写控制，低写，高读
					// RS＝0、RW＝0——表示向LCM写入指令，可以写入指令或者显示地址
					// RS＝0、RW＝1——表示读取Busy标志
					// RS＝1、RW＝0——表示向LCM写入数据
					// RS＝1、RW＝1——表示从LCM读取数据
sbit EN = P1^2;		//使能端，当E端由高电平跳变成低电平时，液晶模块执行命令
#define LCD_DATA P0	//定义传入数据的特殊功能寄存器，或者使用 “static sfr LCD_DATA=0X80;”定义
static unsigned char MaxStrlen = 40;	//定义一行最多显示字符数



/*****函数部分*****/
static void LCD_Delay();
static bit LCD_BUSY(void);
static void Write_cmd(unsigned char cmd);
static void Write_data(unsigned char dat);

/*************************************************
 * @name LCD_Delay()
 * @brief LCD延时函数(12Mhz晶振延时4uS)
 * @param 无
 * @return 无
**************************************************/
static void LCD_Delay()
{
	unsigned char data i;
	i = 10;
	while (--i);
}



/*************************************************
 * @name LCD_BUSY()
 * @brief 检测LCD1602是否处于忙状态
 * @param 无
 * @return 忙信号布尔值（bit）。返回1，则LCD_BUSY；返回0，则OK
**************************************************/
static bit LCD_BUSY(void)
{
	LCD_DATA=0xff;
	RS = 0;
	RW = 1;
	EN = 1;
	LCD_Delay();
	EN = 0;
	return (bit)(LCD_DATA & 0x80); //最高位为忙信号位
}

/*************************************************
 * @name Write_cmd()
 * @brief LCD写一字节命令
 * @param cmd 待写控制命令（unsigned char）
 * @return 无
**************************************************/
static void Write_cmd(unsigned char cmd)
{
	while(LCD_BUSY());   //测忙
	RS = 0;
	RW = 0;
	EN = 1;
	LCD_DATA = cmd;
	LCD_Delay();
	EN = 0;
}

/*************************************************
 * @name Write_data()
 * @brief LCD写一字节数据
 * @param dat 待写显示数据（unsigned char）
 * @return 无
**************************************************/
static void Write_data(unsigned char dat)
{
	while(LCD_BUSY());   //测忙
	RS = 1;
	RW = 0;
	EN = 1;
	LCD_DATA = dat;
	LCD_Delay();
	EN = 0;
}

/*************************************************
 * @name LCD_Init()
 * @brief LCD液晶屏的初始化
 * @param 无
 * @return 无
**************************************************/
void LCD_Init(void)
{
	Write_cmd(0x38);   //功能设置,8位数据接口，两行显示，5×8点阵，即0b00111000也就是0x38
	Write_cmd(0x0c);   //显示开关控制,00001100 显示开，光标不显示，光标不闪烁
	Write_cmd(0x06);   //输入方式设置,00000110 I/D=1：写入新数据后光标右移；S=0：显示不移动。
	Write_cmd(0x01);   //清除LCD的显示内容
	LCD_Clear();
}

/*************************************************
 * @name 	LCD_SetMaxStrlen()
 * @brief 	重新设置LCD屏一行最多显示的字符数
 * @param 	len 待设置的显示数据长度（unsigned char）
 * @return 	返回设置是否成功（bit)。
 * 			返回1，设置成功；
 * 			返回0，设置失败，使用默认值40
**************************************************/
bit LCD_SetMaxStrlen(unsigned char len)
{
	if(len>0&&len<40)
	{
		MaxStrlen = len;
		return 1;
	}
	else
	{
		MaxStrlen = 40;
		return 0;
	}
}

/*************************************************
 * @name LCD_WriteChar()
 * @brief LCD写一个字符
 * @param x 设置显示的X坐标（unsigned char）
 * @param y 设置显示的y坐标（unsigned char）
 * @param ch 待写字符（unsigned char）
 * @return 无
**************************************************/
void LCD_WriteChar(unsigned char x, unsigned char y, unsigned char ch)
{
	unsigned char addr;
	while(x>=MaxStrlen)
	{
		x-=MaxStrlen;
		y++;
	}
	while(y>=2) y-=2;
	if (y == 0)
  	{
    	addr = 0x00 + x; //第一行的x位置显示
  	}
  	else
  	{
    	addr = 0x40 + x; //第二行x的位置显示
  	}
  	Write_cmd(addr + 0x80);
	Write_data(ch);
}

/*************************************************
 * @name LCD_ClsLine()
 * @brief LCD清除行
 * @param line 设置待清除行号（unsigned char）
 * @return 无
**************************************************/
void LCD_ClsLine(unsigned char line)
{
	unsigned char i=0;
	for(i=0;i<MaxStrlen;i++)
		LCD_WriteChar(i,line,' ');
}

/*************************************************
 * @name LCD_Clear()
 * @brief LCD清屏
 * @param 无
 * @return 无
**************************************************/
void LCD_Clear(void)
{
	LCD_ClsLine(0);
	LCD_ClsLine(1);
}

/*************************************************
 * @name LCD_WriteString()
 * @brief LCD写字符串
 * @param x 设置显示的X坐标（unsigned char）
 * @param y 设置显示的y坐标（unsigned char）
 * @param str 待写字符串（unsigned char）
 * @return 字符长度（unsigned char）
**************************************************/
unsigned char LCD_WriteString(unsigned char x, unsigned char y, unsigned char *str)
{
	unsigned char i=0;
	while(str[i]!=0)
	{
		LCD_WriteChar(x, y, str[i]);
		x++;
		i++;
	}
	return i;
}


/*************************************************
 * @name 	LCD_WriteNum()
 * @brief 	LCD写一个数字。
 * 			整数显示——len=数字位数，point=0，num=要显示的数字；
 * 			小数显示——len=num位数，point=小数位数，num=需要显示的小数*10^(point)（即，先将小数放大为整形）
 * @param x 设置显示的X坐标（unsigned char）
 * @param y 设置显示的y坐标（unsigned char）
 * @param len 	待显示的数字位数（unsigned char）
 * @param point 小数点位置（unsigned char）。point=0时，显示整数
 * @param num 	待显示的数字（long）。小数请放大为整数，范围-2147483648~2147483648
 * @return 显示出来的数字所占真实位数（unsigned char)
**************************************************/
unsigned char LCD_WriteNum(unsigned char x, unsigned char y, unsigned char len, unsigned char point, long num)
{
	unsigned char real_len, flag=0;
	if(point) 	len++;
	else		point--;
	if(num<0)
	{
		LCD_WriteChar(x,y,'-');
		len++;
		num=-num;
		flag=1;
	}
	real_len=len;
	while(len-flag)
	{
		if(num>=0) len--;
		if(point)
		{
			LCD_WriteChar(x+len, y, '0'+num%10);
			num/=10;
		}
		else
		{
			LCD_WriteChar(x+len, y, '.');
		}
		point--;
	}
	return real_len;
}

/*************************************************
 * @name 	LCD_WriteHex()
 * @brief 	LCD写一个16进制数字。
 * 			整数显示——len=数字位数，point=0，num=要显示的数字；
 * 			小数显示——len=num位数，point=小数位数，num=需要显示的小数*10^(point)（即，先将小数放大为整形）
 * @param x 设置显示的X坐标（unsigned char）
 * @param y 设置显示的y坐标（unsigned char）
 * @param len 	待显示的16进制数字位数（unsigned char）
 * @param num 	待显示的数字（long）。小数请放大为整数，范围-2147483648~2147483648
 * @param mode  输出显示模式（unsigned char）。
 * 				mode=0时，不生成标记；
 * 				mode=1时，在输出前加上“0X”；
 * 				mode=2时，在输出后加上“H”
 * @return 显示出来的数字所占真实位数（unsigned char)
**************************************************/
unsigned char LCD_WriteHex(unsigned char x, unsigned char y, unsigned char len, long num, unsigned char mode)
{
	unsigned char temp, real_len=len, i=0;
	if(mode == 1)
	{
		LCD_WriteChar(x++, y, '0');
		LCD_WriteChar(x++, y, 'X');
		real_len += 2;
	}
	for(i=0;i<len;i++)
	{
		temp = (num>>((len-1)*4))&0x0f;
		if(temp>=0 && temp<=9)
		{
			LCD_WriteChar(x+i, y, '0'+temp);
		}
		else{
			LCD_WriteChar(x+i, y, 'A'+temp-10);
		}
		num = num<<4;
	}
	if(mode == 2)
	{
		LCD_WriteChar(x+i, y, 'H');
		real_len += 1;
	}
	return real_len;
}

/*************************************************
 * @name 	LCD_WriteBinary()
 * @brief 	LCD显示一个字节的2进制数字
 * @param x 设置显示的X坐标（unsigned char）
 * @param y 设置显示的y坐标（unsigned char）
 * @param num 	待显示的数字（unsigned char）
 * @param mode  输出显示模式（unsigned char）
 * 				mode=0时，不生成标记；
 * 				mode=1时，在输出后加上“B”
 * @return 显示出来的数字所占真实位数（unsigned char)
**************************************************/
unsigned char LCD_WriteBinary(unsigned char x, unsigned char y, unsigned char num, unsigned char mode)
{
	unsigned char real_len=8, i=0;
	for(i = 0; i < 8; i++)
	{
		if((num << i) & 0x80)
		{
			LCD_WriteChar(x + i, y, '1');
		}
		else
		{
			LCD_WriteChar(x + i, y, '0');
		}
	}
	if(mode == 1)
	{
		LCD_WriteChar(x + i, y, 'B');
		real_len += 1;
	}
	return real_len;
}