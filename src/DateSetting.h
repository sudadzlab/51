#ifndef __DATESETTING_H__
#define __DATESETTING_H__


//定义初始日期
#define ORGIN_YEAR		2000
#define ORGIN_MONTH		1
#define ORGIN_DAY		1
#define ORGIN_WEEK		6


unsigned char calWeek( unsigned int year, unsigned char month, unsigned char day);                              //计算当前日期对应的星期
void dayChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode);   //增减日
void monthChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode); //增减月份
void yearChange( unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week, bit mode);  //增减年份


#endif // !__DATESETTING_H__
