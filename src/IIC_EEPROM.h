/** 
* @file         IIC_EEPROM.h 
* @brief        51单片机通过I0模拟I2C总线
* @author       GhpZhu 
* @date     2012/8/21 
* @version  A001 
* @par Copyright (c):  
*       All User 
* @par History:          
*   version: author, date, desc\n 
*/

#ifndef __IIC_EEPROM_H__
#define __IIC_EEPROM_H__

void EEPROM_Write(unsigned char add,unsigned char val);
unsigned char EEPROM_Read(unsigned char add);
void EEPROM_Write16(unsigned char address,unsigned int value);
unsigned int EEPROM_Read16(unsigned char address);

#endif 