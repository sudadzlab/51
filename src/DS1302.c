/**
 * @file DS1302.c
 * @author zx108wl (1696496757@qq.com)
 * @brief 
 * @version 0.1
 * @date 2023-11-07
 * 
 * @copyright Copyright (c) 2023
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
#include "STC15F2K60S2.H"
#include "DS1302.h"
#include "intrins.h"
#include "DateSetting.h"



sbit SCK = P1^7;		
sbit SDA = P2^3;		
sbit RST = P1^3;

// sbit SCK = P4^7;		
// sbit SDA = P4^6;		
// sbit RST = P3^7;

											/*秒	分		时		日		月		星期	年*/
code unsigned char	w_rtc_address[7]	=	{0x80,	0x82,	0x84,	0x86,	0x88,	0x8a,	0x8c}; 	//DS1302写地址
code unsigned char 	r_rtc_address[7]	=	{0x81,	0x83,	0x85,	0x87,	0x89,	0x8b,	0x8d};	//DS1302读地址


#define SCK_Set()		do{SCK=1; _nop_();}while(0)
#define SCK_Clr()		do{SCK=0; _nop_();}while(0)
#define SDA_Set()		do{SDA=1; _nop_();}while(0)
#define SDA_Clr()		do{SDA=0; _nop_();}while(0)
#define SDA_Read()		(SDA)
#define RST_Set()		do{RST=1; _nop_();}while(0)
#define RST_Clr()		do{RST=0; _nop_();}while(0)

static void 			DS1302_WriteByte	( unsigned char temp						);
static void 			DS1302_Write		( unsigned char address, unsigned char dat 	);
static unsigned char 	DS1302_Read			( unsigned char address 					);

/**
 * @name DS1302_WriteByte
 * @brief DS1302写字节
 * 
 * @param temp 待写字节（unsigned char）
 * @return none
 */
static void DS1302_WriteByte( unsigned char temp) 
{
	unsigned char i;
	for (i=0;i<8;i++)     	
	{ 
		SCK_Clr();
		if(temp&0x01)	{SDA_Set();}
		else			{SDA_Clr();}
		temp>>=1; 
		SCK_Set();
	}
}

/**
 * @name DS1302_Write
 * @brief DS1302写数据
 * 
 * @param address 待写入的地址（unsigned char）
 * @param dat  待写入的数据（unsigned char）
 * @return none
 */
static void DS1302_Write( unsigned char address,unsigned char dat )     
{
 	RST_Clr();
 	SCK_Clr();
 	RST_Set(); 
 	DS1302_WriteByte(address);	
 	DS1302_WriteByte(dat);		
 	RST_Clr(); 
}

/**
 * @name DS1302_Read
 * @brief DS1302读数据
 * 
 * @param address 待读取的地址（unsigned char）
 * @return 读取的数据（unsigned char） 
 */
static unsigned char DS1302_Read( unsigned char address )
{
 	unsigned char i,temp=0x00;
 	RST_Clr();
 	SCK_Clr();
 	RST_Set();
 	DS1302_WriteByte(address);
 	for (i=0;i<8;i++) 	
 	{		
		SCK_Clr();
		temp>>=1;	
 		if(SDA_Read())
 		{
			temp|=0x80;	
		}
 		SCK_Set();
	} 
 	RST_Clr();
 	RST_Clr();
	SCK_Clr();
	SCK_Set();
	SDA_Clr();
	SDA_Set();
	return (temp);			
}

/*
*/
void ReadDate(unsigned int *year, unsigned char *month, unsigned char *day, unsigned char *week)
{
	unsigned char temp;
	temp = DS1302_Read(r_rtc_address[6]);
	*year = (temp>>4)*10 + (temp&0x0f);
	*year += ORGIN_YEAR;
	temp = DS1302_Read(r_rtc_address[4]);
	*month = ((temp&0x1f)>>4)*10 + (temp&0x0f);
	temp = DS1302_Read(r_rtc_address[3]);
	*day = ((temp&0x3f)>>4)*10 + (temp&0x0f);
	temp = DS1302_Read(r_rtc_address[5]);
	*week = (temp&0x07);
}
void SetDate(unsigned int year, unsigned char month, unsigned char day)
{
	unsigned char temp;
 	DS1302_Write(0x8E,0X00);	//解除读写保护，允许写入
	year -= ORGIN_YEAR;
	temp = ((year/10)<<4)|(year%10);
	DS1302_Write(w_rtc_address[6], temp);
	temp = ((month/10)<<4)|(month%10);
	temp &= 0x1f;
	DS1302_Write(w_rtc_address[4], temp);
	temp = ((day/10)<<4)|(day%10);
	temp &= 0x3f;
	DS1302_Write(w_rtc_address[3], temp);
	temp = calWeek(year+ORGIN_YEAR, month, day);
	temp = temp?temp:7;
	DS1302_Write(w_rtc_address[5], temp);
	DS1302_Write(0x8E,0x80);	//开启读写保护，禁止写入
}
/**
 * @brief 设置时间显示格式。0：24小时制；1：12小时制
 * 
 * @param mode 设置时间显示格式。0：24小时制；1：12小时制（bit）
 */
void SetRTCMode(bit mode){
	unsigned char temp, hour;
	temp = DS1302_Read(r_rtc_address[2]);
	if(mode){
		if(temp&0x80){
			return;
		}
		else{
			if(temp == 0x00){
				temp = 0x92;
			}else if(temp == 0x12){
				temp = 0xb2;
			}else{
				hour = ((temp&0x3f)>>4)*10 + (temp&0x0f);
				temp = hour>12?((((((hour-12)/10)<<4)|((hour-12)%10))&0x1F)|0xA0):(((((hour/10)<<4)|(hour%10))&0x1F)|0x80);
			}
			DS1302_Write(0x8E,0X00);	//解除读写保护，允许写入
			DS1302_Write(w_rtc_address[2], temp);			
			DS1302_Write(0x8E,0x80);	//开启读写保护，禁止写入
		}
	}else{
		if(temp&0x80){
			hour = ((temp&0x1f)>>4)*10 + (temp&0x0f) + ((temp&0x20)?12:0);
			if(hour == 12 || hour == 24){
				hour -= 12;
			}
			DS1302_Write(0x8E,0X00);	//解除读写保护，允许写入
			temp = ((hour/10)<<4)|(hour%10);
			temp = (/*24*/temp&0x3f);
			DS1302_Write(w_rtc_address[2], temp);			
			DS1302_Write(0x8E,0x80);	//开启读写保护，禁止写入
		}
		else{		
			return;
		}
	}
}

unsigned char ReadRTC(unsigned char *hour, unsigned char *minute, unsigned char *second)
{
	unsigned char temp, flag=0x00;
	temp = DS1302_Read(r_rtc_address[0]);
	*second = ((temp&0x7f)>>4)*10 + (temp&0x0f);
	temp = DS1302_Read(r_rtc_address[1]);
	*minute = ((temp&0x7f)>>4)*10 + (temp&0x0f);
	temp = DS1302_Read(r_rtc_address[2]);
	if(temp&0x80){
		flag = (temp&0x20)?0x03:0x02;
		*hour = ((temp&0x1f)>>4)*10 + (temp&0x0f);
	}
	else{		
		*hour = ((temp&0x3f)>>4)*10 + (temp&0x0f);
	}
	return flag;
}

void SetRTC(unsigned char hour, unsigned char minute, unsigned char second, bit time_mode, bit time, bit run)
{
	unsigned char temp;
 	DS1302_Write(0x8E,0X00);	//解除读写保护，允许写入
	temp = ((second/10)<<4)|(second%10);
	if(run){
		temp = temp&0x7f;
	}else{
		temp = (temp&0x7f)|0x80;
	}
	DS1302_Write(w_rtc_address[0], temp);
	temp = ((minute/10)<<4)|(minute%10);
	DS1302_Write(w_rtc_address[1], temp);
	temp = ((hour/10)<<4)|(hour%10);
	temp = time_mode?((/*12*/(temp|0x80)&0x9f)|(time?0x20:0x00)):(/*24*/temp&0x3f);
	DS1302_Write(w_rtc_address[2], temp);
	
	DS1302_Write(0x8E,0x80);	//开启读写保护，禁止写入
}

// unsigned char* ReadRTC(void)
// {
//  	unsigned char i, *p;
// 	unsigned char tmp[3];
	
//  	p = (unsigned char *)r_rtc_address; 
 	
//  	for(i=0;i<3;i++){
// 	  	tmp[i]=DS1302_Read(*p);
// 	  	p++;
//  	}
	
// 	rtc[0] = (tmp[2] >> 4);
// 	rtc[1] = (tmp[2] & 0x0F);
	
// 	rtc[2] = (tmp[1] >> 4);
// 	rtc[3] = (tmp[1] & 0x0F);
	
// 	rtc[4] = (tmp[0] >> 4);
// 	rtc[5] = (tmp[0] & 0x0F);
	
// 	return rtc;
// }

/*
*/
// void SetRTC(void)
// {
//  	DS1302_Write(0x8E,0X00);	//解除读写保护，允许写入
	
// 	DS1302_Write(w_rtc_address[0], (set[4]<<4) | (set[5]));
// 	DS1302_Write(w_rtc_address[1], (set[2]<<4) | (set[3]));
// 	DS1302_Write(w_rtc_address[2], (set[0]<<4) | (set[1]));
	
// 	DS1302_Write(0x8E,0x80);	//开启读写保护，禁止写入
// }



